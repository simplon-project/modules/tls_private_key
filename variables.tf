# Variables pour le module
variable "keys" {
  description = "Liste des propriétés des clés à créer"
  type = list(object({
    name        = string
    algorithm   = string
    rsa_bits    = optional(number)
    ecdsa_curve = optional(string)
  }))
  default = [
    {
      name      = "key1"
      algorithm = "RSA"
      rsa_bits  = 4096
    },
    {
      name        = "key2"
      algorithm   = "ECDSA"
      ecdsa_curve = "P256"
    }
  ]
}

