# module : TLS PRIVATE KEY

## General

Ce module permet de générer une ou plusieures clé privée ssh en utilisant le fournisseur TLS.

## Description

- **variable.tf**: Ce fichier contient la déclaration des variables utilisées dans le module.

Cette partie déclare une variable `keys` qui est une `liste d’objets`. 

Chaque objet représente une clé à créer et contient les propriétés `name`, `algorithm`, `rsa_bits` et `ecdsa_curve`. 

**Deux clés sont créées par défaut,** `une clé RSA nommée key1` et `une clé ECDSA nommée key2`.

- **main.tf** : Ce fichier contient la définition des ressources à créer.
Pour chaque élément dans var.keys, une clé est créée avec l’algorithme, le nombre de bits RSA et la courbe ECDSA spécifiés

le module crée `un fichier pour chaque clé privée générée`. 
Le contenu du fichier est la clé privée en `format PEM`. 
Le nom du fichier est basé sur le nom de la clé et il est **stocké dans le répertoire ssh du `répertoire racine` du projet**.

- **output.tf** : Ce fichier contient la définition des sorties du module.

La première sortie est la clé privée en format PEM pour chaque clé générée.
a deuxième sortie est la clé publique en format OpenSSH pour chaque clé générée.


## Explication du code

Dans le fichier main.tf La ligne `for_each = { for k in var.keys : k.name => k }` est une expression Terraform qui utilise la boucle `for` pour itérer sur chaque élément de la variable `var.keys`

**elle permet donc de créer une ressource pour chaque élément dans la liste var.keys**.

<u>Voici ce que fait chaque partie de cette expression <u>:

- `for k in var.keys` : Cette partie de l’expression signifie `“pour chaque élément k dans var.keys”`. 
Ici, k est une variable qui représente l’élément actuel de la liste var.keys à chaque itération de la boucle.

- `k.name => k` : Cette partie de l’expression est utilisée pour `créer une nouvelle carte` (un type de données qui associe des valeurs à des clés uniques). 
Pour chaque élément k de var.keys, une nouvelle paire clé-valeur est ajoutée à la carte. 
La clé de la paire est k.name (le nom de la clé actuelle) et la valeur est k (l’élément actuel de var.keys).

- `{ ... }` : Les accolades sont utilisées pour indiquer que l’expression for doit renvoyer une carte. 
Si on souhaite renvoyer une liste, on utilise alors des crochets à la place `[ ... ]`.

- `for_each = ... `: Enfin, l’expression entière est assignée à l’argument for_each de la ressource Terraform. 
Cela signifie que Terraform créera une instance de la ressource pour chaque élément de var.keys. L’identifiant de chaque instance sera le nom de la clé (k.name).

## Analogie 

Imaginons que tu es un chef d’orchestre et que tu as une **liste de musiciens** `var.keys`. 
Chaque musicien a **son propre instrument** à jouer, qui est unique `le name`, et **une partition** spécifique à suivre `algorithm, rsa_bits, ecdsa_curve`.

Maintenant, tu veux que <u>chaque musicien joue sa partition<u>. Pour ce faire, tu utilises une `boucle for` qui est comme donner à chaque musicien l’ordre de commencer à jouer.

L’expression `for k in var.keys : k.name => k` est comme dire : **“Pour chaque musicien dans ma liste, je veux que tu joues ta partition spécifique”**. Ici, `k.name est le nom du musicien` et `k est la partition` que le musicien doit jouer.

Enfin, `for_each = { ... }` est comme dire : **“Je veux que chaque musicien joue en même temps”**. 
C’est pourquoi tu crées une instance de la ressource pour chaque musicien dans ta liste.

## Utilisation

Pour utiliser ce module, il suffit de déclarer le module dans un fichier .tf de votre projet et de spécifier les valeurs des variables requises.

1. **Definir vos clés en utilisant la variable** `keys` que vous déclarez dans votre fichier .tf.

```hcl
variable "keys" {
  default = [
    {
      name      = "nom_de_la_cle"           #id_rsa
      algorithm = "type_de_cle"             #rsa ou ecdsa
      rsa_bits  = "nombre_de_bits_rsa"      #2098 4096
    }
  ]
}
```
Dans cette partie, vous pouvez définir autant de clés que vous le souhaitez en ajoutant des objets à la liste.

2. **Déclarer le module dans votre fichier .tf**

```hcl
module "ssh_keys" {
  source = "git::git@gitlab.com:simplon-project/modules/tls_private_key.git"
  keys   = var.keys
}
```

3. **Utiliser les sorties du module pour obtenir les clés générées**.

```hcl

output "public_keys" {
  value = module.ssh_keys.public_keys_openssh
}
```
Cela affichera les clés publiques de toutes les clés générées après le terraform apply

## Documentation

[TERRAFORM](https://registry.terraform.io/providers/hashicorp/tls/latest/docs/resources/private_key)

## Auteur

[**Kingston-run**](https://gitlab.com/Kingston-run)


