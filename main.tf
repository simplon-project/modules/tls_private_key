# Création des clés SSH
resource "tls_private_key" "ssh_key" {
  for_each = { for k in var.keys : k.name => k }

  algorithm   = each.value.algorithm
  rsa_bits    = each.value.rsa_bits
  ecdsa_curve = each.value.ecdsa_curve
}

# Création des fichiers de clés privées
resource "local_file" "private_ssh_key" {
  for_each = tls_private_key.ssh_key

  content         = each.value.private_key_pem
  filename        = "${path.root}/ssh/${each.key}.pem"
  file_permission = "0600"
}

/*var.keys est une liste de maps, où chaque map contient les propriétés pour une clé
La boucle for_each parcourt chaque élément de la liste et crée une clé SSH et un fichier de clé privée pour chaque map.
tls_private_key.ssh_key et local_file.private_ssh_key sont maintenant des mappages d’objets, 
ce qui signifie que chaque ressource créée a une clé unique */