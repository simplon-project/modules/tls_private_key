# Sorties pour le module
output "private_keys_pem" {
  description = "Les clés privées en format PEM."
  value       = { for k, v in tls_private_key.ssh_key : k => v.private_key_pem }
  sensitive   = true
}

output "public_keys_openssh" {
  description = "Les clés publiques en format OpenSSH."
  value       = { for k, v in tls_private_key.ssh_key : k => v.public_key_openssh }
}